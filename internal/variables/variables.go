package variables

const ReplicaNum int32 = 1

const (
    Image          = "nginx:latest" // Example image
    ContainerPort  = 8080
    ServicePort    = 80
    IngressHost    = "example.com"
)
