package v1

import (
    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// MonitorConfigSpec defines the desired state of MonitorConfig
type MonitorConfigSpec struct {
    APIURL         string `json:"apiUrl"`
    TargetNamespace string `json:"targetNamespace"`
}

// MonitorConfigStatus defines the observed state of MonitorConfig
type MonitorConfigStatus struct {
    LastChecked metav1.Time `json:"lastChecked,omitempty"`
    Success     bool        `json:"success,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

type MonitorConfig struct {
    metav1.TypeMeta   `json:",inline"`
    metav1.ObjectMeta `json:"metadata,omitempty"`

    Spec   MonitorConfigSpec   `json:"spec,omitempty"`
    Status MonitorConfigStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

type MonitorConfigList struct {
    metav1.TypeMeta `json:",inline"`
    metav1.ListMeta `json:"metadata,omitempty"`
    Items           []MonitorConfig `json:"items"`
}

func init() {
    SchemeBuilder.Register(&MonitorConfig{}, &MonitorConfigList{})
}

