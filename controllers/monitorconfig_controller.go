package controllers

import (
    "context"
    "net/http"
    "io/ioutil"
    "strings"
    "time"

    "k8s.io/apimachinery/pkg/runtime"
    ctrl "sigs.k8s.io/controller-runtime"
    "sigs.k8s.io/controller-runtime/pkg/client"
    "sigs.k8s.io/controller-runtime/pkg/log"

    monitorv1 "gitlab.com/optimus3435034/snmp-operator.git"
)

// MonitorConfigReconciler reconciles a MonitorConfig object
type MonitorConfigReconciler struct {
    client.Client
    Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=monitor.mydomain.com,resources=monitorconfigs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=monitor.mydomain.com,resources=monitorconfigs/status,verbs=get;update;patch

func (r *MonitorConfigReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
    _ = log.FromContext(ctx)

    var monitorConfig monitorv1.MonitorConfig
    if err := r.Get(ctx, req.NamespacedName, &monitorConfig); err != nil {
        return ctrl.Result{}, client.IgnoreNotFound(err)
    }

    if !checkAPI(monitorConfig.Spec.APIURL) {
        // Additional logic to manage resources
    }

    // Update status
    monitorConfig.Status.LastChecked = metav1.NewTime(time.Now())
    monitorConfig.Status.Success = true
    if err := r.Status().Update(ctx, &monitorConfig); err != nil {
        return ctrl.Result{}, err
    }

    return ctrl.Result{RequeueAfter: time.Minute}, nil
}

func checkAPI(url string) bool {
    resp, err := http.Get(url)
    if err != nil {
        return false
    }
    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        return false
    }
    return len(strings.Split(string(body), ",")) == 5
}

func (r *MonitorConfigReconciler) SetupWithManager(mgr ctrl.Manager) error {
    return ctrl.NewControllerManagedBy(mgr).
        For(&monitorv1.MonitorConfig{}).
        Complete(r)
}

